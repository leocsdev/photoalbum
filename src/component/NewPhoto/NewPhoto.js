import React, { Component } from 'react';
import styles from './NewPhoto.module.css'

class NewPhoto extends Component {

  render(){
    return (
      <div className={styles.NewPhoto}>
        <h3>Add new Photo</h3>

        <label for="title">Title</label>
        <input id="title" name="title" type="text"></input>

        <label for="url">URL</label>
        <input id="url" name="url" type="text"></input> 

        <label for="album">Album</label>
        <input id="album" name="album" type="text"></input>

        <button>Add Photo</button>
      </div>
    );
  }
}

export default NewPhoto;