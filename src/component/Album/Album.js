import React, { Component } from 'react'
import Photo from '../Photo/Photo'
import styles from './Album.module.css'
import ProfilePhoto from '../ProfilePhoto/ProfilePhoto';
import NewPhoto from '../NewPhoto/NewPhoto';

class Album extends Component {

  render(){
    return (
      <div>
        <ProfilePhoto/>
        <hr />
        <section className={styles.Photos}>
          <Photo />
          <Photo />
          <Photo />
          <Photo />
          <Photo />
          <Photo />
          <Photo />
          <Photo />
        </section>
        <hr />
        <NewPhoto/>
      </div>
    );
  }
}

export default Album;