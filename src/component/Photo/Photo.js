import React from 'react';
import styles from './Photo.module.css'


const Photo = (props) => {
  return (
    <div className={styles.Photo}>
      <img src="https://via.placeholder.com/150/92c952" alt="randomPhoto" />
      <p>accusamus beatae ad facilis cum similique qui sunt</p>
      <button>Set as Profile Photo</button>
    </div>
  );
}


export default Photo;