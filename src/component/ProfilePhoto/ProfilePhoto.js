import React, { Component } from 'react';
import styles from './ProfilePhoto.module.css'


class ProfilePhoto extends Component {

 

  render() {

    let photo = (
       <div className={styles.ProfilePhotoBlank}>
         <p>Please select a profile picture</p>
       </div>
     );

     photo = (
       <img src="https://via.placeholder.com/600/92c952" alt="profilephoto" />
     );
     
    return (
      <div className={styles.ProfilePhoto}>
        {photo}
        <button>Remove</button>
      </div>
    );
  }
}

export default ProfilePhoto;